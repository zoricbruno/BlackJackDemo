package hr.ferit.zoric.blackjack;

public interface IUserInput {
	public boolean getUserSelection();
}
