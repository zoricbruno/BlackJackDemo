package hr.ferit.zoric.blackjack;

public class ConsoleProgressDisplay implements IProgressDisplay {
	
	public ConsoleProgressDisplay() {
	}

	@Override
	public void displayDealerWin() {
		System.out.println("Bummer... You lost, the house wins");		
	}

	@Override
	public void displayPlayerWin() {
		System.out.println("Congratulations! You won!!!");		
	}

	@Override
	public void displayDraw() {
		System.out.println("Draw. Please collect your bets.");		
	}

	@Override
	public void displayHitMePrompt() {
		System.out.println("Hit me? type \"y\" / anything");
	}

	@Override
	public void displayHand(Hand hand) {
		System.out.println(hand);
	}

}
