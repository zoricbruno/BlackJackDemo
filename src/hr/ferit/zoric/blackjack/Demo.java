package hr.ferit.zoric.blackjack;

import java.util.Scanner;

public class Demo {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		IProgressDisplay progressDisplay = new ConsoleProgressDisplay();
		IUserInput userInput = new ConsoleUserInput();
		BlackJackGame game = new BlackJackGame(userInput, progressDisplay);
		
		boolean shouldPlay = true;
		while(shouldPlay){
			shouldPlay = false;
			game.play();
			shouldPlay = shouldPlayAnother(s);
		}
	}
	
	public static boolean shouldPlayAnother(Scanner input){
		System.out.println("Play another game? type \"y\" / anything");
		String line = input.nextLine();
		boolean shouldPlay = line.equals("y") ? true : false;
		return shouldPlay;
	}
}

