package hr.ferit.zoric.blackjack;

public enum Face {	
	ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, KING, QUEEN;
}
