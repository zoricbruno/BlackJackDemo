package hr.ferit.zoric.blackjack;

import java.util.ArrayList;
import java.util.Collections;

public class Deck {
	
	private final String EXCEPTION_NO_CARDS_LEFT = "No cards left in deck.";
	
	private ArrayList<Card> mCards;
	private int mCurrentCard;
	
	public Deck(){
		this.mCurrentCard = 0;
		this.mCards = new ArrayList<>();
		for(Suite suite : Suite.values()){
			for(Face face : Face.values()){
				this.mCards.add(new Card(face, suite));
			}
		}
	}
	
	public void shuffle(){
		Collections.shuffle(this.mCards);
		this.mCurrentCard = 0;
	}
	
	public Card drawCard(){
		if(this.mCurrentCard < this.mCards.size()){
			return this.mCards.get(this.mCurrentCard++);			
		}		
		else{
			throw new BlackJackException.DeckException(EXCEPTION_NO_CARDS_LEFT);
		}			
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(Card card : this.mCards){
			builder.append(card.toString()).append("\n");
		}
		return builder.toString();
	}
}
