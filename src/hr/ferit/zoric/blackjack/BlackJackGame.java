package hr.ferit.zoric.blackjack;

import java.util.Scanner;

public class BlackJackGame{
	
	IProgressDisplay mProgressDisplay;
	IUserInput mUserInput;
	
	private Deck mDeck;
	private Hand mDealer;
	private Hand mPlayer;
	
	public BlackJackGame(IUserInput userInput, IProgressDisplay progressDisplay){
		this.mProgressDisplay = progressDisplay;
		this.mUserInput = userInput;
		this.mDeck = new Deck();
	}
	
	public void play(){
		this.mDealer = new Hand();
		this.mPlayer = new Hand();
		this.mDeck.shuffle();		
		this.dealInitialHands();
		this.playerPlays();
		this.dealerPlays();
		this.determineOutcome();
	}
	
	private void determineOutcome() {
		this.displayPlayerHand();
		this.displayDealerHand();
		System.out.println();
		
		if(this.checkPlayerWin()) {
			this.mProgressDisplay.displayPlayerWin();
		}		
		else if(this.isDraw()) {
			this.mProgressDisplay.displayDraw();
		}
		else {
			this.mProgressDisplay.displayDealerWin();
		}
	}

	private boolean checkPlayerWin() {
		
		if(this.mPlayer.getHandValue() < 22) {
			
			if(this.mDealer.getHandValue() > 21) { return true;	}
			
			if(this.mPlayer.getHandValue() > this.mDealer.getHandValue()) { return true; }
			
			if (this.mPlayer.getHandValue() == this.mDealer.getHandValue()) {
				if(this.mPlayer.getHandCount() < this.mDealer.getHandCount()) {
					return true;
				}
			}						
		}		
		
		return false;
	}

	private boolean isDraw() {
		if(this.mPlayer.getHandValue() == this.mDealer.getHandValue()) {
			if(this.mPlayer.getHandCount() == this.mDealer.getHandCount()) {
				return true;
			}
		}
		return false;
	}

	private void dealerPlays() {
		mDealer.addDrawnCard(mDeck.drawCard());
		while(mDealer.getHandValue() < 17){
			mDealer.addDrawnCard(mDeck.drawCard());
		}		
	}

	private void playerPlays() {		
		while(promptHitMe()){
			mPlayer.addDrawnCard(mDeck.drawCard());
			if(mPlayer.getHandValue() > 21){
				break;
			}						
		}			
	}

	private void dealInitialHands() {
		mPlayer.addDrawnCard(mDeck.drawCard());
		mPlayer.addDrawnCard(mDeck.drawCard());
		mDealer.addDrawnCard(mDeck.drawCard());		
	}
	
	private void displayPlayerHand(){
		this.mProgressDisplay.displayHand(this.mPlayer);
	}
	
	private void displayDealerHand(){
		this.mProgressDisplay.displayHand(this.mDealer);
	}

	private boolean promptHitMe() {
		this.displayPlayerHand();
		this.mProgressDisplay.displayHitMePrompt();
		return this.mUserInput.getUserSelection();
	}
	
}
